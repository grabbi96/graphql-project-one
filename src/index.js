import { GraphQLServer } from 'graphql-yoga';

//  // scalar types
// String
// ID
// Boolean, int, float


// type definitions (schema)
const typeDefs = `
    type Query {
        greeting(name: String):String!,
        me: User!,
        post: Post!,
        grades:[Int!]!
    }

    type User {
        id:ID!,
        name:String!,
        email:String!,
        age:Int!,
        employed: Boolean!,
        gpa:Float
    }


     type Post {
        id:ID!,
        title:String!,
        body:String!,
        published: Boolean!
    }
`;

// Resolve
const resolvers = {
    Query: {
        greeting(parent, args, ctx, info) {
            console.log(args);
            return `Hello ${args.name}`;
        },
        me() {
            return {
                id: "Sadf",
                name: "Rabbi",
                email: "grabbi96@gmail.com",
                age: 28,
                employed: true
            };
        },

        post() {
            return {
                id: "post1323",
                title: "javascript 2020 feature",
                body: "I am ready to get new feature of javascript",
                published: true
            };
        },

        grades(parent, args, ctx, info) {
            return [85, 45, 85];
        }



    }
};


const server = new GraphQLServer({
    typeDefs,
    resolvers,
});


server.start(() => {
    console.log('The server is up!');
});